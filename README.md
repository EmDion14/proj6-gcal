# Progect 7
Grab appointment data from a selection of a user's Google calendars 


## Author(s)
Original: Michal Young
Add ons: Emmalie Dion
ix url: home/users/emmalie/proj6-gcal
Bitbucket repository: https://EmDion14@bitbucket.org/EmDion14/proj6-gcal.git

##IMPORTANT NOTE:
once you choose the calendars the page seems to reset. If you choose the same
calendars again the application will display the busy times.

## What the program is supposed to do

Upon loading the page the user is offered the choice to select a daterange.
Beginning date, ending date, beginning time, and ending time. Given this 
daterange the application brings up a list of all of the calendars that the
user has in Google Calendars. Once the user chooses a calendar or calendars
the application will display the times that the user is busy according to the 
calendars selected and the date range chosen.

##What I have completed

In my application the user is able to select a daterange (dates and times)
and will then be redirected to the page where it displays the list of
Google Calendars. It displays the list of calendars and allows the user to click
which calendars they would like to use. Once the user chooses the calendar the application
will display the times that the user is busy between the date and time ranges on 
those calendars.

#Project 8

## Author(s)
Original: Michal Young
Add ons: Emmalie Dion
ix url: home/users/emmalie/proj6-gcal
Bitbucket repository: https://EmDion14@bitbucket.org/EmDion14/proj6-gcal.git

##IMPORTANT NOTE:
once you choose the calendars the page seems to reset. If you choose the same
calendars again the application will display the busy and available times.

## What the program is supposed to do

Upon loading the page the user is offered the choice to select a daterange.
Beginning date, ending date, beginning time, and ending time. Given this 
daterange the application brings up a list of all of the calendars that the
user has in Google Calendars. Once the user chooses a calendar or calendars
the application will display the times that the user is busy according to the 
calendars selected and the date range chosen, as well as the times that the
user is available between those dates and times.

##What I have completed

In my application the user is able to select a daterange (dates and times)
and will then be redirected to the page where it displays the list of
Google Calendars. It displays the list of calendars and allows the user to click
which calendars they would like to use. Once the user chooses the calendar the application
will display the times that the user is busy between the date and time ranges on 
those calendars, as well as the times that the user is available.


#Project 10

## Author(s)
Original: Michal Young
Add ons: Emmalie Dion
ix url: home/users/emmalie/proj6-gcal
Bitbucket repository: https://EmDion14@bitbucket.org/EmDion14/proj6-gcal.git

##IMPORTANT NOTE:
once you choose the calendars the page seems to reset. If you choose the same
calendars again the application will display the busy and available times.

## What the program is supposed to do

Upon loading the page the user is offered the choice to select a daterange.
Beginning date, ending date, beginning time, and ending time. Given this 
daterange the application brings up a list of all of the calendars that the
user has in Google Calendars. Once the user chooses a calendar or calendars
the application will display the times that the user is busy according to the 
calendars selected and the date range chosen, as well as the times that the
user is available between those dates and times.

##What I have completed

In my application the user is able to select a daterange (dates and times)
and will then be redirected to the page where it displays the list of
Google Calendars. It displays the list of calendars and allows the user to click
which calendars they would like to use. Once the user chooses the calendar the application
will display the times that the user is busy between the date and time ranges on 
those calendars, as well as the times that the user is available.
