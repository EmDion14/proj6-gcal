import flask
from flask import render_template
from flask import request
from flask import url_for
from flask import jsonify
import uuid

import json
import logging
from logging import FileHandler
from agenda import *

# Date handling 
import arrow # Replacement for datetime, based on moment.js
import datetime # But we still need time
from dateutil import tz  # For interpreting local times


# OAuth2  - Google library implementation for convenience
from oauth2client import client
import httplib2   # used in oauth2 flow

# Google API for services 
from apiclient import discovery

###
# Globals
###
import CONFIG
app = flask.Flask(__name__)
file_handler = FileHandler("errlog")
app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(file_handler)

SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
CLIENT_SECRET_FILE = CONFIG.GOOGLE_LICENSE_KEY  ## You'll need this
APPLICATION_NAME = 'MeetMe class project'

#############################
#
#  Pages (routed from URLs)
#
#############################

@app.route("/")
@app.route("/index")
def index():
  app.logger.debug("Entering index")
  if 'begin_date' not in flask.session:
    init_session_values()
  return render_template('index.html')

@app.route("/choose")
def choose():
    ## We'll need authorization to list calendars 
    ## I wanted to put what follows into a function, but had
    ## to pull it back here because the redirect has to be a
    ## 'return' 
    app.logger.debug("Checking credentials for Google calendar access")
    credentials = valid_credentials()
    if not credentials:
      app.logger.debug("Redirecting to authorization")
      return flask.redirect(flask.url_for('oauth2callback'))

    gcal_service = get_gcal_service(credentials)
    app.logger.debug("Returned from get_gcal_service")
    flask.session['calendars'] = list_calendars(gcal_service)
    return render_template('index.html')

####
#
#  Google calendar authorization:
#      Returns us to the main /choose screen after inserting
#      the calendar_service object in the session state.  May
#      redirect to OAuth server first, and may take multiple
#      trips through the oauth2 callback function.
#
#  Protocol for use ON EACH REQUEST: 
#     First, check for valid credentials
#     If we don't have valid credentials
#         Get credentials (jump to the oauth2 protocol)
#         (redirects back to /choose, this time with credentials)
#     If we do have valid credentials
#         Get the service object
#
#  The final result of successful authorization is a 'service'
#  object.  We use a 'service' object to actually retrieve data
#  from the Google services. Service objects are NOT serializable ---
#  we can't stash one in a cookie.  Instead, on each request we
#  get a fresh serivce object from our credentials, which are
#  serializable. 
#
#  Note that after authorization we always redirect to /choose;
#  If this is unsatisfactory, we'll need a session variable to use
#  as a 'continuation' or 'return address' to use instead. 
#
####

def valid_credentials():
    """
    Returns OAuth2 credentials if we have valid
    credentials in the session.  This is a 'truthy' value.
    Return None if we don't have credentials, or if they
    have expired or are otherwise invalid.  This is a 'falsy' value. 
    """
    if 'credentials' not in flask.session:
      return None

    credentials = client.OAuth2Credentials.from_json(
        flask.session['credentials'])

    if (credentials.invalid or
        credentials.access_token_expired):
      return None
    return credentials


def get_gcal_service(credentials):
  """
  We need a Google calendar 'service' object to obtain
  list of calendars, busy times, etc.  This requires
  authorization. If authorization is already in effect,
  we'll just return with the authorization. Otherwise,
  control flow will be interrupted by authorization, and we'll
  end up redirected back to /choose *without a service object*.
  Then the second call will succeed without additional authorization.
  """
  app.logger.debug("Entering get_gcal_service")
  http_auth = credentials.authorize(httplib2.Http())
  service = discovery.build('calendar', 'v3', http=http_auth)
  app.logger.debug("Returning service")
  return service

@app.route('/oauth2callback')
def oauth2callback():
  """
  The 'flow' has this one place to call back to.  We'll enter here
  more than once as steps in the flow are completed, and need to keep
  track of how far we've gotten. The first time we'll do the first
  step, the second time we'll skip the first step and do the second,
  and so on.
  """
  app.logger.debug("Entering oauth2callback")
  flow =  client.flow_from_clientsecrets(
      CLIENT_SECRET_FILE,
      scope= SCOPES,
      redirect_uri=flask.url_for('oauth2callback', _external=True))
  ## Note we are *not* redirecting above.  We are noting *where*
  ## we will redirect to, which is this function. 
  
  ## The *second* time we enter here, it's a callback 
  ## with 'code' set in the URL parameter.  If we don't
  ## see that, it must be the first time through, so we
  ## need to do step 1. 
  app.logger.debug("Got flow")
  if 'code' not in flask.request.args:
    app.logger.debug("Code not in flask.request.args")
    auth_uri = flow.step1_get_authorize_url()
    return flask.redirect(auth_uri)
    ## This will redirect back here, but the second time through
    ## we'll have the 'code' parameter set
  else:
    ## It's the second time through ... we can tell because
    ## we got the 'code' argument in the URL.
    app.logger.debug("Code was in flask.request.args")
    auth_code = flask.request.args.get('code')
    credentials = flow.step2_exchange(auth_code)
    flask.session['credentials'] = credentials.to_json()
    ## Now I can build the service and execute the query,
    ## but for the moment I'll just log it and go back to
    ## the main screen
    app.logger.debug("Got credentials")
    return flask.redirect(flask.url_for('choose'))

#####
#
#  Option setting:  Buttons or forms that add some
#     information into session state.  Don't do the
#     computation here; use of the information might
#     depend on what other information we have.
#   Setting an option sends us back to the main display
#      page, where we may put the new information to use. 
#
#####

@app.route('/setrange', methods=['POST'])
def setrange():
    """
    User chose a date range with the bootstrap daterange
    widget.
    """
    app.logger.debug("Entering setrange") 
    #get value of begin date
    begind = request.form.get('dstart')
    flask.session['dstart'] = begind
    
    #get value of end date
    endd = request.form.get('dend')
    flask.session['dend'] = endd
    
    #get value of begin time
    begint = request.form.get('tstart')
    flask.session['tstart'] = begint
    
    #get value of end time
    endt = request.form.get('tend')
    flask.session['tend'] = endt
    
    #interpret values of begin date
    flask.session['begin_date'] = interpret_date(begind)
    
    #interpret values of end date
    flask.session['end_date'] = interpret_date(endd)
    
    #interpret values of begin time
    flask.session['begin_time'] = interpret_time(begint)
    
    #interpret values of end time
    flask.session['end_time'] = interpret_time(endt)
    
    
    return flask.redirect(flask.url_for("choose"))
	
def create_Appt():
    """ 
    Grab start and end dates and times from session 
    object and create an Appt
    """

    #create arrow objects from start and end dates stored in session
    start_d = arrow.get(flask.session['begin_date'])
    end_d = arrow.get(flask.session['end_date'])

    # create arrow objects from start and end times stored in session
    st_t = arrow.get(flask.session['begin_time'])
    s = st_t.to('local')
    start_t = s.time()
	
    e_t = arrow.get(flask.session['end_time'])
    e = e_t.to('local')
    end_t = e.time()

    appt_times = []
    #for dates between start and end create agenda between start and end times
    for r in arrow.Arrow.span_range('day', start_d, end_d):
        app.logger.debug("this is r {}".format(r))
        for i in r:
            d = r[0].to('local')
            date = d.date()
            get_appt = Appt(date, start_t, end_t, "other")
            appt_times.append(get_appt)
        app.logger.debug("this is appts {}".format(get_appt))
        return appt_times
    
        
    

####
#
#   Initialize session variables 
#
####

def init_session_values():
    """
    Start with some reasonable defaults for date and time ranges.
    Note this must be run in app context ... can't call from main. 
    """ 
    # Default date span = tomorrow to 1 week from now
    now = arrow.now('local')
    tomorrow = now.replace(days=+1)
    nextweek = now.replace(days=+7)
    flask.session["begin_date"] = tomorrow.floor('day').isoformat()
    flask.session["end_date"] = nextweek.ceil('day').isoformat()
    flask.session["daterange"] = "{} - {}".format(
        tomorrow.format("MM/DD/YYYY"),
        nextweek.format("MM/DD/YYYY"))
    # Default time span each day, 8 to 5
    flask.session["begin_time"] = interpret_time("9:00am")
    flask.session["end_time"] = interpret_time("5:00pm")

def interpret_time( text ):
    """
    Read time in a human-compatible format and
    interpret as ISO format with local timezone.
    May throw exception if time can't be interpreted. In that
    case it will also flash a message explaining accepted formats.
    """
    app.logger.debug("Decoding time '{}'".format(text))
    # time_formats = ["ha", "h:mma",  "h:mm a", "H:mm"]
    time_formats = ["h:mma"]
    try: 
        as_arrow = arrow.get(text, time_formats).replace(tzinfo=tz.tzlocal())
        app.logger.debug("Succeeded interpreting time")
    except:
        app.logger.debug("Failed to interpret time")
        flask.flash("Time '{}' didn't match accepted formats 13:30 or 1:30pm"
              .format(text))
        raise
    return as_arrow.isoformat()

def interpret_date( text ):
    """
    Convert text of date to ISO format used internally,
    with the local time zone.
    """
    try:
      as_arrow = arrow.get(text, "MM/DD/YYYY").replace(
          tzinfo=tz.tzlocal())
    except:
        flask.flash("Date '{}' didn't fit expected format 12/31/2001")
        raise
    return as_arrow.isoformat()

def next_day(isotext):
    """
    ISO date + 1 day (used in query to Google calendar)
    """
    as_arrow = arrow.get(isotext)
    return as_arrow.replace(days=+1).isoformat()

####
#
#  Functions (NOT pages) that return some information
#
####
  
def list_calendars(service):
    """
    Given a google 'service' object, return a list of
    calendars.  Each calendar is represented by a dict, so that
    it can be stored in the session object and converted to
    json for cookies. The returned list is sorted to have
    the primary calendar first, and selected (that is, displayed in
    Google Calendars web app) calendars before unselected calendars.
    """
    app.logger.debug("Entering list_calendars")  
    calendar_list = service.calendarList().list().execute()["items"]
    result = [ ]
    for cal in calendar_list:
        kind = cal["kind"]
        id = cal["id"]
        if "description" in cal: 
            desc = cal["description"]
        else:
            desc = "(no description)"
        summary = cal["summary"]
        # Optional binary attributes with False as default
        selected = ("selected" in cal) and cal["selected"]
        primary = ("primary" in cal) and cal["primary"]
        

        result.append(
          { "kind": kind,
            "id": id,
            "summary": summary,
            "selected": selected,
            "primary": primary
            })
    return sorted(result, key=cal_sort_key)


def cal_sort_key( cal ):
    """
    Sort key for the list of calendars:  primary calendar first,
    then other selected calendars, then unselected calendars.
    (" " sorts before "X", and tuples are compared piecewise)
    """
    if cal["selected"]:
       selected_key = " "
    else:
       selected_key = "X"
    if cal["primary"]:
       primary_key = " "
    else:
       primary_key = "X"
    return (primary_key, selected_key, cal["summary"])
	
@app.route('/get_events')    
def get_events() :
    """
    Given start date, end date, and calendar id retrieve
    events from calendar with corresponding id within the
	time range. Retrieves all events on calendar marked as
	"busy"
    """
    app.logger.debug("get_events triggered in Flask program")
    
	#get user defined start date
    bd = request.args.get("bd", type=str)
	
	#get user defined end date
    ed = request.args.get("ed", type=str)
	
	#get user defined start time
    bt = request.args.get("bt", type=str)
    
	#get user defined end time
    et = request.args.get("et", type=str)
   
	
	#turn dates and times into ISO Format
    begind = interpret_date(bd)
    endd = interpret_date(ed)
    begint = interpret_time(bt)
    endt = interpret_time(et)
    
    #take the dates and split them
    start_date = begind.split("T")
    start_time = begint.split("T")
    #combine the two pieces needed for min date (day and time)
    start_date[1] = start_time[1]
    min_date = "T".join(start_date)
    app.logger.debug("minimum date {}".format(min_date))
	
    #take the date and time and split them
    end_date = endd.split("T")
    end_time = endt.split("T")
    #combine the two pieces needed for max date (day and time)
    end_date[1] = end_time[1]
    max_date = "T".join(end_date)
    app.logger.debug("maximum date {}".format(max_date))
	
	
    app.logger.debug("about to get list")
	#get list of calendar ids of checked calendars
    cals = request.args.getlist("cals")
	
    app.logger.debug("got calendars from AJAX {}".format(cals))
	
    events = []
	
	#for element in list of calendars (chosen by user - checkboxes)
    for i in cals:
        app.logger.debug("enter loop")
        app.logger.debug(i)
		#pull out the list of calendar ids
        cal_i = i
		#convert the list to a form python can understand
        calendars = json.loads(cal_i)
        app.logger.debug("the calendars {}".format(calendars))
        for j in calendars:
		#for elements in list of calendar ids
            app.logger.debug("working on calendar id {}".format(j))
            #make search query to google calendars based on id
            freebusy_query = {
                "timeMin" : min_date,
                "timeMax" : max_date,
                "items" :[
                {
                    "id" : j
                }
                ]
            }
            credentials = valid_credentials()
            gcal_service = get_gcal_service(credentials)
            #send query to google calendars to get information
            req = gcal_service.freebusy().query(body=freebusy_query)
            #get object with busy dates inside
            rslt = req.execute()
            #for key and value in object returned from search query
            for key, value in rslt.items():
                #if object property == calendars
                if key == "calendars":
                    #grab the value containing busy times
                    busy = value[j]["busy"]
                    
                    app.logger.debug("value {}".format(value[j]))
                    app.logger.debug("busy {}".format(busy))
                    #for element in list of busy times
                    for k in busy:
                        events.append(k)    
                    #app.logger.debug("this is events : {}".format(events))    
            #app.logger.debug("getting events {}".format(events))
        #app.logger.debug("about to return {}".format(jsonify(result=events)))
    return jsonify(result=events)
    #rslt = request.execute()
    #rslt = {"begin_date" : bd, "end_date" :ed}


def make_Appt(appt):

    make = appt
    app.logger.debug("this is make {}".format(make))
    s_times = arrow.get(make["start"])
    e_times = arrow.get(make["end"])
    local = s_times.to('local')
    local2 = e_times.to('local')
    date = local.date()
    time_s = local.time()
    time_e = local2.time()
    return Appt(date, time_s, time_e, "busy")
    #app.logger.debug("this is appt {}".format(Appt(date,time_s,time_e,"busy")))
    
@app.route('/get_avail')
def get_avail() :

    busy = request.args.getlist("busy") 
    app.logger.debug("this is busy : {}".format(busy))
    busy_appts = Agenda()
    for i in busy:
        busy_events = i
        ag = json.loads(busy_events)
        app.logger.debug("events in busy {}".format(ag))
        for j in ag:
            busy_appts.append(make_Appt(j))
    
        periods = create_Appt()
        available = Agenda()
        for p in periods:
            av = busy_appts.complement(p)
            for a in av:
                available.append(a)
    app.logger.debug("this is busy_appts {}".format(busy_appts))
    app.logger.debug("this is available {}".format(available))

    return jsonify(rslt=available)
    

#################
#
# Functions used within the templates
#
#################

@app.template_filter( 'fmtdate' )
def format_arrow_date( date ):
    try: 
        normal = arrow.get( date )
        return normal.format("ddd MM/DD/YYYY")
    except:
        return "(bad date)"

@app.template_filter( 'fmttime' )
def format_arrow_time( time ):
    try:
        normal = arrow.get( time )
        return normal.format("HH:mm")
    except:
        return "(bad time)"
    
#############


if __name__ == "__main__":
  # App is created above so that it will
  # exist whether this is 'main' or not
  # (e.g., if we are running in a CGI script)

  app.secret_key = str(uuid.uuid4())  
  app.debug=CONFIG.DEBUG
  app.logger.setLevel(logging.DEBUG)
  # We run on localhost only if debugging,
  # otherwise accessible to world
  if CONFIG.DEBUG:
    # Reachable only from the same computer
    app.run(port=CONFIG.PORT)
  else:
    # Reachable from anywhere 
    app.run(port=CONFIG.PORT,host="0.0.0.0")
    
